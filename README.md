# Pour lancer le projet il faut avoir sur sa machine:

- Visual Studio
- Docker (https://docs.docker.com/get-docker/)
- Postman, si vous voulez lancer les requetes fournies (https://www.postman.com/downloads/)
- Sqlectron si vous voulez visualiser la base de données (https://sqlectron.github.io/)

# Une foi docker lancé il vous suffit de suivre les instructions suivantes:

- Cloner le projet avec "git clone lien" disponible sur gitlab ou télécharger le .zip
- Ouvrir le terminal au niveau du projet (./nom_du_projet/api) et suivre les insctruction suivante

1/ Lancer la commande "npm i" pour installer les dépendances

2/ Dans un terminal lancer la commande "npm run dev" pour lancer l'image de la base de données (postgresql)

2/ Dans un autre terminal lancer la commande "npm start" pour start le serveur

3/ tester l'API grace à postman

4/ Lancer les tests disponibles avec la commande "npm test"

# Schéma de la base de donnée disponible sur : https://dbdiagram.io/d/5f69dce27da1ea736e2ed6b7
# Collection Postman disponoble sur : https://www.getpostman.com/collections/d40a1f233169f022c984 il vous suffira de copier le lien et de le fournir à postman pour pouvoir y accéder (importer => link )
