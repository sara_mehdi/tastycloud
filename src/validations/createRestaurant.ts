import {
  IsString,
  MaxLength,
  IsEmail,
  MinLength,
  IsOptional,
  Matches,
  IsArray,
} from "class-validator";

import {
  ADRESS_LENGTH,
  MAX_FIRSTNAME_LENGTH,
  MAX_LENGTH_EMAIL,
  MAX_NAME_LENGTH,
  MIN_LENGTH_EMAIL,
  PASSWORD_MIN_LENGTH,
  RESTAURANT_NAME_MAX_LENGTH,
  RESTAURANT_NAME_MIN_LENGTH,
  VALID_EMAIL,
  VALID_PHONE,
} from "../helpers/errors";
import Menus from "../models/menus.entity";

const phoneRegExp = /^\+((?:9[679]|8[035789]|6[789]|5[90]|42|3[578]|2[1-689])|9[0-58]|8[1246]|6[0-6]|5[1-8]|4[013-9]|3[0-469]|2[70]|7|1)(?:\W*\d){0,13}\d$/;

export class CreateRestaurant {
  @IsString()
  @IsOptional()
  @MaxLength(50, { message: MAX_FIRSTNAME_LENGTH })
  public firstname: string;

  @IsString()
  @IsOptional()
  @MaxLength(50, { message: MAX_NAME_LENGTH })
  public lastname: string;

  @IsString()
  @IsOptional()
  @MaxLength(500, {
    message: ADRESS_LENGTH,
  })
  public adress: string;

  @IsString()
  @IsEmail(undefined, { message: VALID_EMAIL })
  @MaxLength(50, {
    message: MAX_LENGTH_EMAIL,
  })
  @MinLength(10, {
    message: MIN_LENGTH_EMAIL,
  })
  public email: string;

  @IsString()
  @MinLength(8, {
    message: PASSWORD_MIN_LENGTH,
  })
  public password: string;

  @IsString()
  @MaxLength(200, {
    message: RESTAURANT_NAME_MAX_LENGTH,
  })
  @MinLength(5, {
    message: RESTAURANT_NAME_MIN_LENGTH,
  })
  public name: string;

  @IsString()
  @Matches(phoneRegExp, { message: VALID_PHONE })
  public phoneNumber: string;

  @IsArray()
  @IsOptional()
  public menus: Menus[];
}
