import {
  IsString,
  MaxLength,
  MinLength,
  IsNumber,
  IsPositive,
  IsArray,
  IsOptional,
} from "class-validator";

import Drinks from "../models/drinks.entity";
import Products from "../models/products.entity";
import Menus from "../models/menus.entity";

export class UpdateFormula {
  @IsOptional()
  @IsString()
  @MaxLength(50, {
    message: "Le nom de la formule ne doit pas dépasser 50 caractères",
  })
  @MinLength(3, {
    message: "Le nom de la formule doit contenir au minimum 3 caractères",
  })
  public name: string;

  @IsOptional()
  @IsNumber()
  @IsPositive()
  public price: number;

  @IsOptional()
  public drink: Drinks;

  @IsOptional()
  @IsArray()
  public products: Products[];

  @IsOptional()
  public menu: Menus;
}
