import {
  IsString,
  MaxLength,
  IsNumber,
  MinLength,
  IsPositive,
} from "class-validator";

import { drinkTypeEnum } from "../models/drinks.entity";
import Menus from "../models/menus.entity";

export class CreateDrink {
  @IsString()
  @MaxLength(50, {
    message: "Le nom de la boisson ne doit pas dépasser 50 caractères ",
  })
  @MinLength(5, {
    message: "Le nom de la boisson doit contenir au minimum 5 caractères",
  })
  public name: string;

  @IsNumber()
  @IsPositive()
  public price: number;

  @IsString()
  @MaxLength(50, {
    message: "Le type de la boisson ne doit pas dépasser 50 caractères",
  })
  public type: drinkTypeEnum;

  public menu: Menus;
}
