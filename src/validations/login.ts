import {
  IsEmail,
  MaxLength,
  MinLength,
  IsString,
  IsAlphanumeric,
} from "class-validator";

export class Login {
  @IsEmail()
  @MaxLength(100, {
    message: "L'adresse e-mail ne doit pas dépssser 100 caractères",
  })
  @MinLength(5, {
    message: "L'adresse e-mail doit contenir au minimum 5 caractères ",
  })
  public email: string;

  @IsString()
  @MinLength(8, {
    message: "Le mot de passe doit conteni au minimum 8 caractères",
  })
  @IsAlphanumeric(
    "Le mot de passe doit etre alphanumérique, avec au moin un chiffre"
  )
  public password: string;
}
