import { IsArray, IsOptional } from "class-validator";

import Products from "../models/products.entity";
import Formulas from "../models/formulas.entity";
import Drinks from "../models/drinks.entity";
import Restaurants from "../models/restaurants.entity";

export class CreateMenu {
  @IsArray()
  @IsOptional()
  public links: string[];

  @IsArray()
  @IsOptional()
  public products: Products[];

  @IsOptional()
  @IsArray()
  public formulas: Formulas[];

  @IsOptional()
  @IsArray()
  public drinks: Drinks[];

  public restaurant: Restaurants;
}
