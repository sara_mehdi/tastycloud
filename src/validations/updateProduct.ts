import {
  IsString,
  MaxLength,
  IsNumber,
  IsArray,
  IsOptional,
  MinLength,
  IsPositive,
} from "class-validator";

import { productEnum } from "../models/products.entity";
import Ingredients from "../models/ingrediants.entity";
import Menus from "../models/menus.entity";

export class UpdateProduct {
  @IsOptional()
  @IsString()
  @MaxLength(50, {
    message: "Le nom du produit ne doit pas dépasser 50 caractères",
  })
  @MinLength(5, {
    message: "Le nom du produit doit contenir au minimum 5 caractères",
  })
  public name: string;

  @IsNumber()
  @IsPositive()
  @IsOptional()
  public price: number;

  @IsString()
  @IsOptional()
  @MaxLength(50, {
    message: "Le type du produit ne doit pas dépasser 50 caracètres",
  })
  public type: productEnum;

  @IsArray()
  @IsOptional()
  public ingredients: Ingredients[];

  public menu: Menus;
}
