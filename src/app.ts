import "reflect-metadata";
import express, { Express } from "express";
import cors from "cors";
import helmet from "helmet";
import bodyParser from "body-parser";
import {
  RoutingControllersOptions,
  useExpressServer,
} from "routing-controllers";

import { authorizationChecker, currentUserChecker } from "./auth";

class App {
  public app: Express = express();

  constructor() {
    this.config();
  }

  public config() {
    this.app.enable("trust proxy");
    this.app.set("port", process.env.PORT || 3000);
    this.app.use(
      cors({
        origin: [process.env.APP_FRONT!],
      })
    );
    this.app.use(helmet());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    const routingControllersOptions: RoutingControllersOptions = {
      routePrefix: "",
      controllers: [__dirname + "/controllers/**/*.ts"],
      authorizationChecker: authorizationChecker(),
      currentUserChecker: currentUserChecker(),
    };
    useExpressServer(this.app, routingControllersOptions);
  }
}

export default App;
