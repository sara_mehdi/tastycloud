import "reflect-metadata";
import { Action, UnauthorizedError } from "routing-controllers";
import jwt from "jsonwebtoken";
import { getCustomRepository } from "typeorm";

import { RestaurantsRepository } from "./repositories/restaurant.repository";
import { INVALID_TOKEN, TOKEN_NOT_PROVIDED } from "./helpers/errors";
import { secret } from "./helpers/constants";

export const authorizationChecker = () => {
  return async (action: Action, roles: string[]) => {
    const token = action.request.headers["authorization"];
    if (!token) {
      console.log(TOKEN_NOT_PROVIDED);
      throw new UnauthorizedError(TOKEN_NOT_PROVIDED);
    }

    try {
      jwt.verify(token, secret);
    } catch (error) {
      throw new UnauthorizedError(INVALID_TOKEN);
    }

    const restaurant = await getCustomRepository(RestaurantsRepository).findOne(
      {
        where: { authToken: token },
      }
    );
    if (!restaurant) {
      throw new UnauthorizedError(INVALID_TOKEN);
    }
    if (restaurant) return true;

    return false;
  };
};

export const currentUserChecker = () => {
  return async (action: Action) => {
    const token = action.request.headers.authorization;

    try {
      jwt.verify(token, secret);
    } catch (err) {
      throw new UnauthorizedError(INVALID_TOKEN);
    }

    if (token === undefined) {
      console.log(TOKEN_NOT_PROVIDED);
      throw new UnauthorizedError(TOKEN_NOT_PROVIDED);
    }
    const user = await getCustomRepository(RestaurantsRepository).findByToken(
      token
    );

    return user;
  };
};
