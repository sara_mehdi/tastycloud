export const MAX_LENGTH_EMAIL =
  "L'adresse e-mail ne doit pas dépasser 50 caractères";

export const MIN_LENGTH_EMAIL =
  "L'adresse e-mail est requise et doit contenir au minimum 10 caractères";

export const MAX_FIRSTNAME_LENGTH =
  "Le prénom ne doit pas dépasser 50 caractères";

export const MAX_NAME_LENGTH = "Le nom ne doit pas dépasser 50 caractères";

export const ADRESS_LENGTH =
  "L'adresse postale ne doit pas dépasser 500 caractères";

export const PASSWORD_MIN_LENGTH =
  "Le mot de passe doit contenir au minimum 8 caractères";

export const RESTAURANT_NAME_MAX_LENGTH =
  "Le nom de l'établissement ne doit pas dépasser 200 caractères";

export const RESTAURANT_NAME_MIN_LENGTH =
  "Le nom de l'établissement doit contenir au minimum 5 caractères";

export const VALID_PHONE = "Le numéro de téléphone n'est pas valide";

export const VALID_EMAIL = "L'adresse e-mail n'est pas valide";

export const RESTAURANT_ALREADY_EXIST =
  "Un restaurant de ce nom existe déjà à cette adresse";

export const EMAIL_ALREADY_USED = "Cette adresse e-mail a déjà été associé";

export const WRONG_PASSWORD = "Le mot de passe est érroné";

export const WRONG_EMAIL = "L'adresse e-mail est érronée";

export const TOKEN_NOT_PROVIDED = "Token not provided";

export const INVALID_TOKEN = "Invalid token";

export const MENU_NOT_FOUND = "Menu not found, wrong id";

export const PRODUCT_NOT_FOUND = "Product not found, wrong id";

export const DRINK_NOT_FOUND = "Drink not found, wrong id";

export const FORMULA_NOT_FOUND = "Formula not found, wrong id";

export const RESTAURANT_NOT_FOUND = "Restaurant not found, wrong id";

export const INGREDIENT_NOT_FOUND = "Ingredient not found, wrong id";
