import { Service } from "typedi";
import { EntityRepository, Repository } from "typeorm";
import { NotFoundError } from "routing-controllers";

import Ingredients from "../models/ingrediants.entity";
import { INGREDIENT_NOT_FOUND } from "../helpers/errors";

@Service()
@EntityRepository(Ingredients)
export class IngredientsRepository extends Repository<Ingredients> {
  public async createIngredient(name: string): Promise<Ingredients> {
    const ingredient = new Ingredients();
    ingredient.name = name;
    ingredient.available = true;

    return await this.save(ingredient);
  }

  public async isAvailable(id: number) {
    const ingredient = await this.findOne(id);
    return ingredient !== undefined
      ? ingredient.available
      : new NotFoundError("Ingredient not found, wrong id");
  }

  public async unavailable(id: number) {
    await this.findAndUpdate(id, { available: false });
  }

  public async findAndUpdate(id: number, data: any) {
    const ingredient = await this.findOne(id);

    if (!ingredient) {
      throw new NotFoundError(INGREDIENT_NOT_FOUND);
    }
    await this.update(id, data);
    return { message: "Ingredient updated" };
  }
}
