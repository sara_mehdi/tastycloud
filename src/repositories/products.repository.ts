import { Service } from "typedi";
import { EntityRepository, Repository } from "typeorm";
import { NotFoundError } from "routing-controllers";

import Products from "../models/products.entity";
import Menus from "../models/menus.entity";
import { PRODUCT_NOT_FOUND } from "../helpers/errors";

@Service()
@EntityRepository(Products)
export class ProductRepository extends Repository<Products> {
  public async createProduct(data: Products, menu: Menus) {
    const product = new Products();
    product.name = data.name;
    product.price = data.price;
    product.type = data.type;
    product.menu = menu;
    product.available = true;
    product.ingredients = data.ingredients;

    return await this.save(product);
  }

  public async isAvailable(id: number) {
    const product = await this.findOne(id);
    return product !== undefined
      ? product.available
      : new NotFoundError("Product not found, wrong id");
  }

  public async unavailable(id: number, idprod: number) {
    await this.findAndUpdate(id, idprod, { available: false });
  }

  public async findAndUpdate(id: number, idprod: number, data: any) {
    const product = this.findOne(idprod, {
      where: { menu: id },
      relations: ["ingredients", "menu"],
    });
    if (!product) {
      throw new NotFoundError(PRODUCT_NOT_FOUND);
    }

    await this.update(idprod, data);
    return { message: "Product updated" };
  }
}
