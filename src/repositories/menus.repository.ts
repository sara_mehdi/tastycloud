import { NotFoundError } from "routing-controllers";
import { Service } from "typedi";
import { EntityRepository, Repository } from "typeorm";

import { MENU_NOT_FOUND } from "../helpers/errors";
import Menus from "../models/menus.entity";
import Restaurants from "../models/restaurants.entity";

@Service()
@EntityRepository(Menus)
export class MenusRepository extends Repository<Menus> {
  public async createMenu(
    data: Menus,
    restaurant: Restaurants
  ): Promise<Menus> {
    const menu = new Menus();
    menu.links = data.links;
    menu.products = data.products;
    menu.formulas = data.formulas;
    menu.drinks = data.drinks;
    menu.restaurant = restaurant;

    return await this.save(menu);
  }

  public async findAndUpdate(restaurant: Restaurants, id: number, data: any) {
    const menu = await this.findOne(id, {
      where: { restaurant },
      relations: ["drinks", "products", "formulas", "restaurant"],
    });

    if (!menu) {
      throw new NotFoundError(MENU_NOT_FOUND);
    }
    await this.update(id, data);
    return { message: "Menu updated" };
  }
}
