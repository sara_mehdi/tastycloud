import { Service } from "typedi";
import { EntityRepository, Repository } from "typeorm";

import Drinks from "../models/drinks.entity";
import Menus from "../models/menus.entity";

@Service()
@EntityRepository(Drinks)
export class DrinksRepository extends Repository<Drinks> {
  public async createDrink(data: Drinks, menu: Menus): Promise<Drinks> {
    const drink = new Drinks();
    drink.name = data.name;
    drink.price = data.price;
    drink.type = data.type;
    drink.menu = menu;

    return await this.save(drink);
  }

  public async findAndUpdate(id: number, idDrink: number, data: any) {
    const drink = await this.findOne(id);
    return drink !== undefined
      ? (await this.update(idDrink, data), { message: "Drink updated" })
      : "Drink not found, wrong id";
  }
}
