import { Service } from "typedi";
import { EntityRepository, Repository } from "typeorm";

import Formulas from "../models/formulas.entity";
import Menus from "../models/menus.entity";

@Service()
@EntityRepository(Formulas)
export class FormulasRepository extends Repository<Formulas> {
  public async createFormula(data: Formulas, menu: Menus): Promise<Formulas> {
    const formula = new Formulas();
    formula.name = data.name;
    formula.price = data.price;
    formula.drink = data.drink;
    formula.products = data.products;
    formula.menu = menu;

    return await this.save(formula);
  }

  public async findAndUpdate(id: number, idformula: number, data: any) {
    const formula = await this.findOne(idformula, {
      where: { menu: id },
      relations: ["drink", "products", "menu"],
    });
    return formula !== undefined
      ? (this.update(idformula, data), { message: "Formula updated" })
      : "Formula not found, wrong id";
  }
}
