import "reflect-metadata";
import { Service } from "typedi";
import { EntityRepository, Repository } from "typeorm";
import { NotFoundError } from "routing-controllers";

import Restaurants from "../models/restaurants.entity";

@Service()
@EntityRepository(Restaurants)
export class RestaurantsRepository extends Repository<Restaurants> {
  public async createRestaurant(data: Restaurants) {
    const restaurant = new Restaurants();
    restaurant.firstname = data.firstname;
    restaurant.lastname = data.lastname;
    restaurant.adress = data.adress;
    restaurant.email = data.email;
    restaurant.password = data.password;
    restaurant.name = data.name;
    restaurant.phoneNumber = data.phoneNumber;

    return await this.save(restaurant);
  }

  public async getMenus(id: number) {
    const restaurant = await this.findOne(id);
    return restaurant !== undefined
      ? restaurant.menus
      : new NotFoundError("Restaurant not found, wrong id");
  }

  public async findAndUpdate(id: number, data: Restaurants) {
    const restaurant = await this.findOne(id);

    return restaurant !== undefined
      ? (await this.update(id, data), { message: "Restaurant updated" })
      : new NotFoundError("Restaurant not found, wrong id");
  }

  public async alreadyExist(adress: string, name: string) {
    const restaurant = await this.findOne({ where: { adress, name } });
    return restaurant !== undefined;
  }

  public async findByEmail(email: string) {
    return await this.findOne({ where: { email } });
  }

  public async findByToken(token: string) {
    return await this.findOne({ where: { authToken: token } });
  }
}
