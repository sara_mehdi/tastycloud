import status from "http-status";
import {
  Connection,
  createConnection,
  useContainer as ormUseContainer,
} from "typeorm";
import { useContainer as routingUseContainer } from "routing-controllers";
import Container from "typedi";
import { assign, findIndex } from "lodash";
import request from "supertest";

import App from "../app";
import {
  MAX_LENGTH_EMAIL,
  MIN_LENGTH_EMAIL,
  VALID_EMAIL,
  TOKEN_NOT_PROVIDED,
  INVALID_TOKEN,
} from "../helpers/errors";

describe("Restaurants tests", () => {
  let connection: Connection;
  const app = new App().app;

  beforeAll(async () => {
    routingUseContainer(Container);
    ormUseContainer(Container);
    connection = await createConnection();
  });

  afterEach(async () => {
    await connection.dropDatabase();
    await connection.synchronize();
  });

  afterAll(async () => {
    await connection.close();
  });

  describe("Post restaurant", () => {
    const restaurant = {
      firstname: "tasty",
      lastname: "cloud",
      adress: "somewhere over the rainbow",
      email: "tasty@cloud.com",
      password: "Password14",
      name: "Comme chez paul",
      phoneNumber: "+33760416883",
    };

    describe("Check if email is valid", () => {
      it("Should respond an error of invalid email", async (done) => {
        const newRestauran = assign({}, restaurant);
        newRestauran.email = "tasty.cloud.fr";

        const response = await request(app)
          .post("/restaurants")
          .send(newRestauran)
          .set("Accept", "application/json");

        expect(response.status).toEqual(status.BAD_REQUEST);

        const indexError = findIndex(response.body.errors, {
          property: "email",
        });
        expect(indexError).not.toEqual(-1);

        expect(response.body.errors[indexError].constraints).toEqual({
          isEmail: VALID_EMAIL,
        });
        done();
      });
    });

    describe("Check response when email's length not valid", () => {
      it("Should response with a bad request error about maximum length", async (done) => {
        const newRestaurant = assign({}, restaurant);
        newRestaurant.email =
          "tastyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy@clouuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuud.com";

        const response = await request(app)
          .post("/restaurants")
          .send(newRestaurant)
          .set("Accept", "application/json");

        expect(response.status).toEqual(status.BAD_REQUEST);

        const indexError = findIndex(response.body.errors, {
          property: "email",
        });
        expect(indexError).not.toEqual(-1);

        expect(response.body.errors[indexError].constraints).toEqual({
          maxLength: MAX_LENGTH_EMAIL,
        });
        done();
      });
    });

    describe("Verify response when min length not respected", () => {
      it("Should response with a bad request error about minimum length ", async (done) => {
        const newRestaurant = assign({}, restaurant);
        newRestaurant.email = "ta@d.fr";

        const response = await request(app)
          .post("/restaurants")
          .send(newRestaurant)
          .set("Accept", "application/json");

        expect(response.status).toEqual(status.BAD_REQUEST);

        const indexError = findIndex(response.body.errors, {
          property: "email",
        });

        expect(indexError).not.toEqual(-1);

        expect(response.body.errors[indexError].constraints).toEqual({
          minLength: MIN_LENGTH_EMAIL,
        });
        done();
      });
    });

    describe("Check response when udefined email", () => {
      it("Should repond with bad request error when undefined email", async (done) => {
        const newRestaurant = assign({}, restaurant);
        newRestaurant.email = " ";

        const response = await request(app)
          .post("/restaurants")
          .set("Accept", "application/json");

        expect(response.status).toEqual(status.BAD_REQUEST);

        const indexError = findIndex(response.body.errors, {
          property: "email",
        });

        expect(indexError).not.toEqual(-1);

        expect(response.body.errors[indexError].constraints).toEqual({
          isEmail: VALID_EMAIL,
          maxLength: MAX_LENGTH_EMAIL,
          minLength: MIN_LENGTH_EMAIL,
          isString: "email must be a string",
        });
        done();
      });
    });

    describe("Verify response to double creation", () => {
      it("Should responed with a bad request error to already existant restaurant", async (done) => {
        await request(app)
          .post("/restaurants")
          .send(restaurant)
          .set("Accept", "application/json");

        const response = await request(app)
          .post("/restaurants")
          .send(restaurant)
          .set("Accept", "application/json");

        expect(response.status).toEqual(status.BAD_REQUEST);

        done();
      });
    });

    describe("Verify login", () => {
      it("Should respond with a bad resquest to invalid password", async (done) => {
        await request(app)
          .post("/restaurants")
          .send(restaurant)
          .set("Accept", "application/json");

        const res = await request(app)
          .post("/restaurants/login")
          .send({ email: restaurant.email, password: "toto" })
          .set("Accept", "application.json");
        expect(res.status).toEqual(status.BAD_REQUEST);

        done();
      });
      it("Should respond with a bad resquest to invalid email", async (done) => {
        await request(app)
          .post("/restaurants")
          .send(restaurant)
          .set("Accept", "application/json");

        const res = await request(app)
          .post("/restaurants/login")
          .send({ email: "some@email.com", password: restaurant.password })
          .set("Accept", "application.json");
        expect(res.status).toEqual(status.BAD_REQUEST);

        done();
      });
    });
  });

  describe("Restaurants get", () => {
    describe("Verify get all restaurants without login", () => {
      it("Should return an unauthorized error", async (done) => {
        const response = await request(app).get("/restaurants");
        expect(response.status).toEqual(status.UNAUTHORIZED);

        expect(response.body.message).toEqual(TOKEN_NOT_PROVIDED);

        const res = await request(app)
          .get("/restaurants")
          .set("Authorization", "Somewrongtokenvalue");

        expect(res.status).toEqual(status.UNAUTHORIZED);

        expect(res.body.message).toEqual(INVALID_TOKEN);

        done();
      });
    });
  });
});
