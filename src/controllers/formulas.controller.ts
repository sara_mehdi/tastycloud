import status from "http-status";
import {
  JsonController,
  Post,
  HttpCode,
  Body,
  BadRequestError,
  Get,
  Param,
  Put,
  Delete,
  NotFoundError,
  Authorized,
} from "routing-controllers";
import { InjectRepository } from "typeorm-typedi-extensions";

import { CreateFormula } from "../validations/createFormula";
import { UpdateFormula } from "../validations/updateFormula";
import { FormulasRepository } from "../repositories/formulas.repository";
import Formulas from "../models/formulas.entity";
import { MenusRepository } from "../repositories/menus.repository";
import { FORMULA_NOT_FOUND, MENU_NOT_FOUND } from "../helpers/errors";

@JsonController("/menu/:id/formulas")
export class FormulasController {
  @InjectRepository(FormulasRepository)
  private formulaRepo: FormulasRepository;
  @InjectRepository(MenusRepository)
  private menuRepo: MenusRepository;

  @Authorized()
  @Post()
  @HttpCode(status.CREATED)
  public async createFormula(
    @Param("id") id: number,
    @Body() body: CreateFormula
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      this.formulaRepo.createFormula(body as Formulas, menu);
      return { message: "Formula created" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get()
  @HttpCode(status.OK)
  public async getFormulas(@Param("id") id: number) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      return this.formulaRepo.find({
        where: { menu },
        relations: ["menu", "products", "drink"],
      });
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get("/:idformula")
  @HttpCode(status.OK)
  public async getFormula(
    @Param("id") id: number,
    @Param("idformula") idformula: number
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      return this.formulaRepo.findOne(idformula, {
        where: { menu: id },
        relations: ["drink", "products", "menu"],
      });
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Put("/:idformula")
  @HttpCode(status.OK)
  public async updateFormula(
    @Param("id") id: number,
    @Param("idformula") idformula: number,
    @Body() body: UpdateFormula
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }
      const formula = await this.formulaRepo.findOne(idformula);
      if (!formula) {
        throw new NotFoundError(FORMULA_NOT_FOUND);
      }

      return this.formulaRepo.findAndUpdate(id, idformula, body);
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Delete("/:idformula")
  @HttpCode(status.OK)
  public async deleteFormula(
    @Param("id") id: number,
    @Param("idformula") idformula: number
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      const formula = await this.formulaRepo.findOne(idformula, {
        where: { menu: id },
      });
      if (!formula) {
        throw new NotFoundError(FORMULA_NOT_FOUND);
      }

      this.formulaRepo.delete(idformula);
      return { message: "Formula deleted" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }
}
