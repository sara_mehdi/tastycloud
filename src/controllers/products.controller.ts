import status from "http-status";
import {
  JsonController,
  Post,
  HttpCode,
  Body,
  Authorized,
  BadRequestError,
  Put,
  Get,
  Param,
  NotFoundError,
  Delete,
} from "routing-controllers";
import { InjectRepository } from "typeorm-typedi-extensions";

import { ProductRepository } from "../repositories/products.repository";
import Products from "../models/products.entity";
import { CreateProduct } from "../validations/createProduct";
import { UpdateProduct } from "../validations/updateProduct";
import { MENU_NOT_FOUND, PRODUCT_NOT_FOUND } from "../helpers/errors";
import { MenusRepository } from "../repositories/menus.repository";

@JsonController("/menu/:id/products")
export class ProductsController {
  @InjectRepository(ProductRepository)
  private productsRepo: ProductRepository;
  @InjectRepository(MenusRepository)
  private menuRepo: MenusRepository;

  @Authorized()
  @Post()
  @HttpCode(status.CREATED)
  public async createProduct(
    @Param("id") id: number,
    @Body() body: CreateProduct
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      await this.productsRepo.createProduct(body as Products, menu);
      return { message: "Product created" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get()
  @HttpCode(status.OK)
  public async getProducts(@Param("id") id: number) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      const products = await this.productsRepo.find({
        where: { menu },
        relations: ["ingredients", "menu"],
      });

      return products;
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get("/:idprod")
  @HttpCode(status.OK)
  public async getProduct(
    @Param("id") id: number,
    @Param("idprod") idprod: number
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      return this.productsRepo.findOne(idprod, {
        where: { menu },
        relations: ["ingredients", "menu"],
      });
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Put("/:idprod")
  @HttpCode(status.OK)
  public async updateProduct(
    @Param("id") id: number,
    @Param("idprod") idprod: number,
    @Body() body: UpdateProduct
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }
      return this.productsRepo.findAndUpdate(id, idprod, body);
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Delete("/:idprod")
  @HttpCode(status.OK)
  public async deleteProduct(
    @Param("id") id: number,
    @Param("idprod") idprod: number
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      const product = await this.productsRepo.findOne(idprod, {
        where: { menu: id },
      });
      if (!product) {
        throw new NotFoundError(PRODUCT_NOT_FOUND);
      }

      this.productsRepo.delete(idprod);
      return { message: "Product deleted" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }
}
