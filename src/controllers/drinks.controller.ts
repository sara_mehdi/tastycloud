import status from "http-status";
import {
  JsonController,
  Post,
  HttpCode,
  Body,
  Get,
  Param,
  Put,
  Delete,
  Authorized,
  BadRequestError,
  NotFoundError,
} from "routing-controllers";
import { InjectRepository } from "typeorm-typedi-extensions";

import { DrinksRepository } from "../repositories/drinks.repository";
import { CreateDrink } from "../validations/createDrink";
import Drinks from "../models/drinks.entity";
import { UpdateDrink } from "../validations/updateDrink";
import { MenusRepository } from "../repositories/menus.repository";
import { DRINK_NOT_FOUND, MENU_NOT_FOUND } from "../helpers/errors";

@JsonController("/menu/:id/drinks")
export class DrinksController {
  @InjectRepository(DrinksRepository)
  private drinksRepo: DrinksRepository;
  @InjectRepository(MenusRepository)
  private menuRepo: MenusRepository;

  @Authorized()
  @Post()
  @HttpCode(status.CREATED)
  public async createDrink(@Param("id") id: number, @Body() body: CreateDrink) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      this.drinksRepo.createDrink(body as Drinks, menu);
      return { message: "Drink created" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get()
  @HttpCode(status.OK)
  public async getDrinks(@Param("id") id: number) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      return this.drinksRepo.find({ where: { menu: id }, relations: ["menu"] });
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get("/:idDrink")
  @HttpCode(status.OK)
  public async getDrink(
    @Param("id") id: number,
    @Param("idDrink") idDrink: number
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      return this.drinksRepo.findOne(idDrink, {
        where: { menu },
        relations: ["menu"],
      });
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Put("/:idDrink")
  @HttpCode(status.OK)
  public async updateDrink(
    @Param("id") id: number,
    @Param("idDrink") idDrink: number,
    @Body() body: UpdateDrink
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      return this.drinksRepo.findAndUpdate(id, idDrink, body);
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Delete("/:idDrink")
  @HttpCode(status.OK)
  public async deleteDrink(
    @Param("id") id: number,
    @Param("idDrink") idDrink: number
  ) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      const drink = await this.drinksRepo.findOne(idDrink, {
        where: { menu: id },
        relations: ["menu"],
      });
      if (!drink) {
        throw new NotFoundError(DRINK_NOT_FOUND);
      }

      this.drinksRepo.delete({ id: idDrink, menu });
      return { message: "Drink deleted" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }
}
