import "reflect-metadata";
import status from "http-status";
import {
  JsonController,
  Post,
  HttpCode,
  Body,
  InternalServerError,
  Get,
  Param,
  Put,
  Delete,
  Authorized,
} from "routing-controllers";
import { InjectRepository } from "typeorm-typedi-extensions";

import Ingredients from "../models/ingrediants.entity";
import { IngredientsRepository } from "../repositories/ingredients.repository";

@JsonController("/ingredients")
export class IngredientsController {
  @InjectRepository(IngredientsRepository)
  private ingredientRepo: IngredientsRepository;

  @Authorized()
  @Post()
  @HttpCode(status.CREATED)
  public async createIngredient(@Body() body: Ingredients) {
    try {
      this.ingredientRepo.createIngredient(body.name);
      return { message: "Ingredient created" };
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  @Authorized()
  @Get()
  @HttpCode(status.OK)
  public getIngredients() {
    try {
      return this.ingredientRepo.find();
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  @Authorized()
  @Get("/:id")
  @HttpCode(status.OK)
  public getIngredient(@Param("id") id: number) {
    try {
      return this.ingredientRepo.findOne(id);
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  @Authorized()
  @Put("/:id")
  @HttpCode(status.OK)
  public updateIngredient(@Param("id") id: number, @Body() body: Ingredients) {
    try {
      return this.ingredientRepo.findAndUpdate(id, body);
    } catch (error) {
      throw new InternalServerError(error);
    }
  }

  @Authorized()
  @Delete("/:id")
  @HttpCode(status.OK)
  public deleteIngredient(@Param("id") id: number) {
    try {
      this.ingredientRepo.delete(id);
      return { message: "Ingredient deleted" };
    } catch (error) {
      throw new InternalServerError(error);
    }
  }
}
