import status from "http-status";
import {
  JsonController,
  Post,
  HttpCode,
  Body,
  Get,
  Param,
  Put,
  Delete,
  NotFoundError,
  Authorized,
  CurrentUser,
  BadRequestError,
} from "routing-controllers";
import { InjectRepository } from "typeorm-typedi-extensions";

import { MenusRepository } from "../repositories/menus.repository";
import Menus from "../models/menus.entity";
import { CreateMenu } from "../validations/createMenu";
import { UpdateMenu } from "../validations/updateMenu";
import Restaurants from "../models/restaurants.entity";
import { MENU_NOT_FOUND } from "../helpers/errors";

@Authorized()
@JsonController("/menus")
export class MenusController {
  @InjectRepository(MenusRepository)
  private menuRepo: MenusRepository;

  @Authorized()
  @Post()
  @HttpCode(status.CREATED)
  public createMenu(
    @CurrentUser({ required: true }) restaurant: Restaurants,
    @Body() body: CreateMenu
  ) {
    try {
      this.menuRepo.createMenu(body as Menus, restaurant);
      return { message: "Menu created" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get()
  @HttpCode(status.OK)
  public async getMenus() {
    try {
      return this.menuRepo.find({
        relations: ["drinks", "products", "formulas", "restaurant"],
      });
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get("/:id")
  @HttpCode(status.OK)
  public async getMenu(@Param("id") id: number) {
    try {
      return await this.menuRepo.findOne(id, {
        relations: ["drinks", "products", "formulas", "restaurant"],
      });
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Put("/:id")
  @HttpCode(status.OK)
  public async updateMenu(@Param("id") id: number, @Body() body: UpdateMenu) {
    try {
      const menu = await this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      this.menuRepo.update(id, body);
      return { message: "Menu updated" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Delete("/:id")
  @HttpCode(status.OK)
  public async deleteMenu(@Param("id") id: number) {
    try {
      const menu = this.menuRepo.findOne(id);
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      this.menuRepo.delete(id);
      return { message: "Menu deleted" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }
}
