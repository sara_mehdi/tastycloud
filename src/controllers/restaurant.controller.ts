import status from "http-status";
import jwt from "jsonwebtoken";
import {
  JsonController,
  Post,
  HttpCode,
  Body,
  Get,
  Param,
  Put,
  Delete,
  NotFoundError,
  BadRequestError,
  Authorized,
  CurrentUser,
} from "routing-controllers";
import { InjectRepository } from "typeorm-typedi-extensions";

import { RestaurantsRepository } from "../repositories/restaurant.repository";
import Restaurants from "../models/restaurants.entity";
import { CreateRestaurant } from "../validations/createRestaurant";
import { UpdateRestaurant } from "../validations/updateRestaurant";
import { Login } from "../validations/login";
import {
  EMAIL_ALREADY_USED,
  MENU_NOT_FOUND,
  RESTAURANT_ALREADY_EXIST,
  RESTAURANT_NOT_FOUND,
  WRONG_EMAIL,
  WRONG_PASSWORD,
} from "../helpers/errors";
import { MenusRepository } from "../repositories/menus.repository";
import { UpdateMenu } from "../validations/updateMenu";

@JsonController("/restaurants")
export class RestaurantController {
  @InjectRepository(RestaurantsRepository)
  private restaurantRepo: RestaurantsRepository;
  @InjectRepository(MenusRepository)
  private menusRepo: MenusRepository;

  @Post()
  @HttpCode(status.CREATED)
  public async register(@Body() body: CreateRestaurant) {
    try {
      const alreadyExist = await this.restaurantRepo.alreadyExist(
        body.adress,
        body.name
      );
      if (alreadyExist) {
        throw new BadRequestError(RESTAURANT_ALREADY_EXIST);
      }

      const emailAlreadyUsed = await this.restaurantRepo.findByEmail(
        body.email
      );
      if (emailAlreadyUsed) {
        throw new BadRequestError(EMAIL_ALREADY_USED);
      }

      this.restaurantRepo.createRestaurant(body as Restaurants);
      return { message: "Restaurant created" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Post("/login")
  @HttpCode(status.OK)
  public async login(@Body({ validate: false }) body: Login) {
    try {
      const restaurant = await this.restaurantRepo.findByEmail(body.email);
      if (!restaurant) {
        throw new NotFoundError(WRONG_EMAIL);
      }
      const isValidPassword = await restaurant?.isValidPassword(body.password);

      if (!isValidPassword) {
        throw new NotFoundError(WRONG_PASSWORD);
      }

      const payload = {
        id: restaurant.id,
        firstname: restaurant.firstname,
        lastname: restaurant.lastname,
        email: restaurant.email,
      };
      const authToken = jwt.sign({ user: payload }, "TASTYCLOUD", {
        expiresIn: "1d",
      });
      this.restaurantRepo.findAndUpdate(restaurant.id, {
        authToken,
      } as Restaurants);
      return { message: "Welcome to Tasty Cloud :)", authToken };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get()
  @HttpCode(status.OK)
  public getRestaurants() {
    try {
      return this.restaurantRepo.find();
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get("/:id")
  @HttpCode(status.OK)
  public getRestaurant(@Param("id") id: number) {
    try {
      return this.restaurantRepo.findOne(id);
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Put("/:id")
  @HttpCode(status.OK)
  public async updateRestaurant(
    @Param("id") id: number,
    @Body() body: UpdateRestaurant
  ) {
    try {
      return this.restaurantRepo.findAndUpdate(id, body as Restaurants);
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Delete("/:id")
  @HttpCode(status.OK)
  public async deleteRestaurant(@Param("id") id: number) {
    try {
      const restaurant = await this.restaurantRepo.findOne(id);
      if (!restaurant) {
        throw new NotFoundError(RESTAURANT_NOT_FOUND);
      }

      await this.restaurantRepo.delete(id);
      return { message: "Restaurant deleted" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Put("/logout/auth")
  @HttpCode(status.OK)
  public async logout(
    @CurrentUser({ required: true }) restaurant: Restaurants
  ) {
    try {
      await this.restaurantRepo.update(restaurant, {
        authToken: "",
      });
      return { message: "Bye see ya :)" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Get("/list/my/menus")
  @HttpCode(status.OK)
  public async getMyMenus(
    @CurrentUser({ required: true }) restaurant: Restaurants
  ) {
    try {
      return this.menusRepo.find({
        where: { restaurant },
        relations: ["drinks", "products", "formulas", "restaurant"],
      });
    } catch (error) {
      throw new BadRequestError(error);
    }
  }
  @Authorized()
  @Get("/menu/:id")
  @HttpCode(status.OK)
  public async getMenu(
    @CurrentUser({ required: true }) restaurant: Restaurants,
    @Param("id") id: number
  ) {
    try {
      return await this.menusRepo.findOne(id, {
        where: { restaurant },
        relations: ["drinks", "products", "formulas", "restaurant"],
      });
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Put("/menu/:id")
  @HttpCode(status.OK)
  public async updateMenu(
    @CurrentUser({ required: true }) restaurant: Restaurants,
    @Param("id") id: number,
    @Body() body: UpdateMenu
  ) {
    try {
      return this.menusRepo.findAndUpdate(restaurant, id, body);
    } catch (error) {
      throw new BadRequestError(error);
    }
  }

  @Authorized()
  @Delete("/menu/:id")
  @HttpCode(status.OK)
  public async deleteMenu(
    @CurrentUser({ required: true }) restaurant: Restaurants,
    @Param("id") id: number
  ) {
    try {
      const menu = this.menusRepo.findOne(id, { where: { restaurant } });
      if (!menu) {
        throw new NotFoundError(MENU_NOT_FOUND);
      }

      this.menusRepo.delete(id);
      return { message: "Menu deleted" };
    } catch (error) {
      throw new BadRequestError(error);
    }
  }
}
