import "reflect-metadata";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  CreateDateColumn,
  UpdateDateColumn,
  AfterInsert,
  AfterUpdate,
  AfterRemove,
  ManyToOne,
  JoinTable,
} from "typeorm";

import Products from "./products.entity";
import Formulas from "./formulas.entity";
import Drinks from "./drinks.entity";
import Restaurants from "./restaurants.entity";

@Entity()
export default class Menus {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: "varchar", length: 200, nullable: true })
  public links: string[];

  @ManyToMany(() => Products, (product) => product.id, { nullable: true })
  @JoinTable()
  public products: Products[];

  @ManyToMany(() => Formulas, (formula) => formula.id, { nullable: true })
  @JoinTable()
  public formulas: Formulas[];

  @ManyToMany(() => Drinks, (drink) => drink.id, { nullable: true })
  @JoinTable()
  public drinks: Drinks[];

  @ManyToOne(() => Restaurants, (restaurant) => restaurant.id)
  public restaurant: Restaurants;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @AfterInsert()
  private insertMenu() {
    console.log("The Menu has been inserted");
  }

  @AfterUpdate()
  private updateMenu() {
    console.log("The Menu has been updated");
  }

  @AfterRemove()
  private removeMenu() {
    console.log("The Menu has been removed");
  }
}
