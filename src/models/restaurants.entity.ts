import "reflect-metadata";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate,
  AfterInsert,
  AfterUpdate,
  OneToMany,
} from "typeorm";
import bcrypt from "bcryptjs";

import Menus from "./menus.entity";

@Entity()
export default class Restaurants {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: "varchar", length: 200, nullable: false })
  public name: string;

  @Column({ type: "varchar", length: 500, nullable: false })
  public adress: string;

  @Column({ type: "varchar", length: 25 })
  public firstname: string;

  @Column({ type: "varchar", length: 25 })
  public lastname: string;

  @Column({ type: "varchar", length: 20 })
  public phoneNumber: string;

  @Column({ type: "varchar", length: 100, nullable: false })
  public email: string;

  @Column({ type: "varchar", length: 100, nullable: false })
  public password: string;

  @OneToMany(() => Menus, (menu) => menu.restaurant, { nullable: true })
  public menus: Menus[];

  @Column({ type: "varchar", nullable: true })
  public authToken: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public UpdatedAt: Date;

  @BeforeInsert()
  @BeforeUpdate()
  private async hashPassword() {
    if (this.password) this.password = await bcrypt.hash(this.password, 10);
  }

  public isValidPassword(password: string) {
    return bcrypt.compare(password, this.password);
  }

  @BeforeInsert()
  private emailToLowerCase() {
    this.email = this.email.toLowerCase();
  }

  @AfterInsert()
  private insertRestaurant() {
    console.log("Your restaurant has been created");
  }

  @AfterUpdate()
  private updtateRestaurant() {
    console.log("Your restaurant has been updated");
  }
}
