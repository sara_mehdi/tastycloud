import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  AfterInsert,
  AfterUpdate,
  AfterRemove,
  ManyToOne,
} from "typeorm";
import { IsString } from "class-validator";

import Menus from "./menus.entity";

export enum drinkTypeEnum {
  JUICE = "JUICE",
  SODA = "SODA",
  WATER = "WATER",
  SPARKLINGWATTER = "SPARKLINGWATTER",
  BEER = "BEER",
  COCKTAIL = "COCKTAIL",
  WINE = "WINE",
  LIQUOR = "LIQUOR",
}

@Entity()
export default class Drinks {
  @PrimaryGeneratedColumn()
  public id: number;

  @IsString()
  @Column({ type: "varchar", length: 50 })
  public name: string;

  @Column({ type: "varchar", length: 50 })
  public type: drinkTypeEnum;

  @Column({ type: "float" })
  public price: number;

  @ManyToOne(() => Menus, (menu) => menu.id)
  public menu: Menus;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public UpdatedAt: Date;

  @AfterInsert()
  private logInsertDrink() {
    console.log("Drink has been inserted");
  }

  @AfterUpdate()
  private logUpdateDrink() {
    console.log("Drink has been updated");
  }

  @AfterRemove()
  private removeDrink() {
    console.log("The drink has been removed");
  }
}
