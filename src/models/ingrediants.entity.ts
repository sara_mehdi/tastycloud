import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  AfterUpdate,
  AfterInsert,
  AfterRemove,
} from "typeorm";

@Entity()
export default class Ingredients {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: "varchar", length: 100, unique: true })
  public name: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @Column({ type: "boolean" })
  public available: boolean;

  @AfterInsert()
  private insertIng() {
    console.log("Ingredient has been inserted");
  }

  @AfterUpdate()
  private updateIng() {
    console.log("Ingredient has been updated");
  }

  @AfterRemove()
  private removeIngredient() {
    console.log("The ingredient has been removed");
  }
}
