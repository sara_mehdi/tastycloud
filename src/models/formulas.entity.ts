import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  CreateDateColumn,
  UpdateDateColumn,
  AfterInsert,
  AfterUpdate,
  AfterRemove,
  ManyToOne,
  JoinTable,
} from "typeorm";

import Products from "./products.entity";
import Drinks from "./drinks.entity";
import Menus from "./menus.entity";

@Entity()
export default class Formulas {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: "varchar", length: 100 })
  public name: string;

  @Column({ type: "float" })
  public price: number;

  @ManyToMany(() => Products, (product) => product.id)
  @JoinTable()
  public products: Products[];

  @ManyToOne(() => Drinks, (drink) => drink.id)
  public drink: Drinks;

  @CreateDateColumn()
  public createdAt: Date;

  @ManyToOne(() => Menus, (menu) => menu.id)
  public menu: Menus;

  @UpdateDateColumn()
  public updatedAt: Date;

  @AfterInsert()
  private insertFormulas() {
    console.log("The formulas has been inserted");
  }

  @AfterUpdate()
  private updateFormulas() {
    console.log("The formulas has been updated");
  }

  @AfterRemove()
  private removeFormula() {
    console.log("The formula has been removed");
  }
}
