import "reflect-metadata";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  CreateDateColumn,
  UpdateDateColumn,
  AfterInsert,
  AfterUpdate,
  AfterRemove,
  ManyToOne,
  JoinTable,
} from "typeorm";

import { IsString } from "class-validator";
import Ingredients from "./ingrediants.entity";
import Menus from "./menus.entity";

export enum productEnum {
  MAIN = "Plat principal",
  DESSERT = "Dessert",
  ENTREE = "Entrée",
  APER = "Apéritif",
}

@Entity()
export default class Products {
  @PrimaryGeneratedColumn()
  public id: number;

  @IsString()
  @Column({ type: "varchar", length: 100 })
  public name: string;

  @Column({ type: "varchar", length: 50 })
  public type: productEnum;

  @Column({ type: "float" })
  public price: number;

  @Column({ type: "boolean" })
  public available: boolean;

  @ManyToOne(() => Menus, (Menus) => Menus.id)
  public menu: Menus;

  @ManyToMany(() => Ingredients, (Ingredients) => Ingredients.id, {
    nullable: true,
    cascade: ["insert", "update"],
  })
  @JoinTable()
  public ingredients: Ingredients[];

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @AfterInsert()
  private insertProduct() {
    console.log("The product has been inserted");
  }

  @AfterUpdate()
  private updateProduct() {
    console.log("The product has been updated");
  }

  @AfterRemove()
  private removeProduct() {
    console.log("The product has been removed");
  }
}
