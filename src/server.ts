import "reflect-metadata";
import { createConnection, useContainer as ormUseContainer } from "typeorm";
import { useContainer as routingUseContainer } from "routing-controllers";
import { useContainer as cronUseContainer } from "cron-decorators";
import { Container } from "typedi";
import { EventEmitter } from "events";

import App from "./app";

const dataBaseConnexion = async () => {
  try {
    routingUseContainer(Container);
    cronUseContainer(Container);
    ormUseContainer(Container);

    await createConnection();

    console.log("Database launched on port 5432");
  } catch (err) {
    console.log(err);
  }
};

export const server = async () => {
  try {
    const app = new App().app;
    EventEmitter.defaultMaxListeners = 0;
    await app.listen(app.get("port"));
    console.log(`Server is up on port ${app.get("port")}`);
  } catch (error) {
    console.log(error);
  }
};

dataBaseConnexion();
server();
