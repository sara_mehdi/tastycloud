FROM node:12.13.0 as build

WORKDIR /usr/src/app

COPY . .
RUN npm install && npm run build

FROM node:12.13.0 as runtime
WORKDIR /usr/src/app

COPY --from=build /usr/src/app/dist ./
COPY --from=build ["/usr/src/app/package.json", "/usr/src/app/package-lock.json", "./"]

RUN npm install 

EXPOSE 3000
CMD [ "node", "./src/server.ts" ]